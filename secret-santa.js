const nodemailer = require('nodemailer')
const csv = require('csv-parser')
const fetch = require('node-fetch')
const fs = require('fs')
const config = require('./config')
const dateformat = require('dateformat')


if (!['none', 'one', 'all', 'off'].filter(e => config.testMode == e).length) {
    console.log('wrong testMode');
    process.exit();
}

const test = config.testMode != 'off';

var people = [];
var pairsOriginal = [];
const sheetLink = 'https://docs.google.com/spreadsheets/d/' + config.sheetId + '/gviz/tq?tqx=out:csv&sheet=' + config.sheetName;
const logFile = 'secret-santa.' + dateformat(new Date(), 'yyyy-mm-dd HH-MM-ss') + '.log';


var transporter = nodemailer.createTransport({
    service: 'Yandex',
    auth: {
        user: config.smtp.login,
        pass: config.smtp.pasword
    },
    tls: {
        rejectUnauthorized: false
    }
});

Array.prototype.diff = function (a) {
    return this.filter(function (i) {
        return a.indexOf(i) < 0;
    });
};

function sendMail(person) {
    const to = test ? person : person.target; //test
    const testText = '<span>Это тестовая рассылка. Вот такое письмо придёт тому, кому выпадет поздравлять тебя. Убедись, что всё правильно!</span><br><hr>';
    const mailOptions = {
        from: 'Тайный санта ' + config.sheetName + ' <secret-santa@bronyakin.ru>',
        to: person.name + ' <' + person.email + '>',
        subject: 'Тайный санта ' + config.sheetName + (test ? '. Тестовая рассылка' : ''),
        html: (test ? testText : '') +
            '<span style="font-size:14px">Привет, ' + person.name + '!' +
            '<br>Твой тайный поздравляемый — <b>' + to.name + '.</b>' +
            '<br>Вот ему (ей) (не)повезло!' +
            ('undefined' != typeof to.vk ? '<br><b>vk: </b>' + to.vk : '') +
            ('undefined' != typeof to.address ? '<br><b>Адрес: </b>' + to.address : '') +
            '<br><br><b>Вишлист:</b><br>' + to.wishlist +
            '<br><br><b>Антивишлист:</b><br>' + to.antiwishlist +
            ('undefined' != typeof person.bring ? '<br><br><b>Ты обещал принести:</b><br>' + person.bring : '') +
            '<br><br><span style="font-size:10px;color:#333333">Пожалуйста, не тяни с поиском и отправкой подарка!' +
            '<br>Чем ближе праздник, тем больше ажиотаж в магазинах' + ('undefined' != typeof to.address ? ' и на почте' : '') + '!</span>' +
            '</span>'
    };
    fs.appendFileSync(logFile, JSON.stringify((mailOptions), null, 4));

    if ('none' == config.testMode) {
        return;
    }
    if ('one' == config.testMode) {
        if (person.email != config.testEmail) {
            return;
        }
    }

    return transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            throw (error);
        } else {
            console.log('Отправлено: ' + person.email + ' ' + info.response);
        }
    });
}

(async () => {
    const res = await fetch(sheetLink);
    res.body.pipe(csv(config.cols))
        .on('data', data => {
            data.id = +data.id;
            if (data.id > 0 && data.email.length) {

                data.nointersect += ',' + data.id;
                data.nointersect = data.nointersect.trim().replace(/\n|\r/g, '');
                data.nointersect = data.nointersect.split(',').map(e => +e);

                data.name = data.name.trim();
                data.email = data.email.trim();
                if ('undefined' != typeof data.address) {
                    data.address = data.address.trim().replace(/\n|\r/g, ' ');
                }
                if ('undefined' != typeof data.vk) {
                    data.vk = data.vk.trim();
                }
                if ('undefined' != typeof data.bring) {
                    data.bring = data.bring.trim().replace(/\n/g, '<br>');
                }
                data.wishlist = data.wishlist.trim().replace(/\n/g, '<br>');
                data.antiwishlist = data.antiwishlist.trim().replace(/\n/g, '<br>');
                people.push(data);
                pairsOriginal.push(data.id);
            }
        }).on('end', () => {
            let restart = false;
            //console.log(people);
            do {
                let pairs = pairsOriginal;
                people.sort((a, b) => b.nointersect.length - a.nointersect.length);
                people.filter(e => {
                    var candidates = pairs.diff(e.nointersect);
                    if (candidates.length) {
                        let r = Math.floor(Math.random() * candidates.length);
                        let candidate = candidates[r];
                        e.target = people.filter(p => p.id == candidate)[0]; //candidate;
                        pairs = pairs.diff([candidate]);
                    } else {
                        restart = true;
                    }
                });
            } while (restart);
            fs.appendFileSync(logFile, JSON.stringify(people.map(e => ({
                fromEmail: e.email,
                fromId: e.id,
                toId: e.target.id,
                toName: e.target.name
            })), null, 4));

            Promise.all(people.map(p => sendMail(p))).then(() => console.log('complete'))
        });
})();